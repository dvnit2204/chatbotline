<?php

use App\Http\Controllers\GoogleAccountController;
use App\Http\Controllers\RichMenuLineController;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post('/created-rich', [RichMenuLineController::class, 'createMenu']);
Route::get('/', [RichMenuLineController::class, 'getListMenu']);
Route::get('/detail-rich-menu', [RichMenuLineController::class, 'detailMenu']);
Route::get('/upload-image', [RichMenuLineController::class, 'uploadImageRichMenu']);
Route::get('/rep', [RichMenuLineController::class, 'repMessage']);
Route::get('oauth2callback', [
    'as' => 'oauth',
    'uses' => 'OAuthController@index'
]);

Route::get('google', [GoogleAccountController::class, 'index'])->name('google.index');
Route::get('google/oauth', 'GoogleAccountController@store')->name('google.store');
Route::delete('google/{googleAccount}', 'GoogleAccountController@destroy')->name('google.destroy');


