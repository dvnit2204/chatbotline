<?php

use App\Http\Controllers\BookingController;
use App\Http\Controllers\RichMenuLineController;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Spatie\GoogleCalendar\Event;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('/created-rich', [RichMenuLineController::class, 'createMenu']);
Route::get('/home', [RichMenuLineController::class, 'getListMenu']);
Route::get('/detail-rich-menu', [RichMenuLineController::class, 'detailMenu']);
Route::post('/upload-image', [RichMenuLineController::class, 'uploadImageRichMenu']);
Route::delete('/delete-rich-menu', [RichMenuLineController::class, 'deleteMenu']);

Route::get('/events', [BookingController::class, 'listCalendar']);
Route::post('/event/booking', [BookingController::class, 'createCalendar']);
Route::post('/event/{id}/update', [BookingController::class, 'updateCalendar']);
Route::delete('/event/{id}/delete', [BookingController::class, 'deleteCalendar']);

Route::post('/rep-message', [RichMenuLineController::class, 'repMessage']);

Route::post('/webhook', [RichMenuLineController::class, 'webhook']);
