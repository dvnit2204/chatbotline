<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'line' => [
        'token' => env('LINE_CHANNEL_TOKEN', '9TaKU4rPQjfhWNqw2O+ltYgSvg5N0p8OxCAzfn+hbw1gO0m5rXKENS/TsZDqqHfol3L924A8j193n2hz8qggDLFB9wuIstJvX+0jirERXC6YGgj4mKniCNgqVOpeov95q+9z+xSwXJVpmKtX2MHrZAdB04t89/1O/w1cDnyilFU='),
        'secret' => env('LINE_CHANNEL_SECRET', 'e45cd3dc7cac342027208c504cb5ae5b'),
    ]

];
