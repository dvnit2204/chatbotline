<?php
namespace App\Services;

use Carbon\Carbon;
use Spatie\GoogleCalendar\Event;

class BookingService
{
    public function list()
    {
        return Event::get();
    }
    public function create($params)
    {
        $startTime = Carbon::parse($params['meeting_date'] . ' ' . $params['meeting_time'], config('app.timezone'));
        $endTime = (clone $startTime)->addHour();
        $evt = Event::create([
            'name' => $params['name'],
            'startDateTime' => $startTime,
            'endDateTime' => $endTime
        ]);
        dd($evt);
        return $evt;
    }
    public function update($eventId, $params)
    {
        $event = Event::find($eventId);
        $startTime = Carbon::parse($params['meeting_date'] . ' ' . $params['meeting_time'], config('app.timezone'));
        $endTime = (clone $startTime)->addHour();
        $event->update([
            'name' => $params['name'],
            'startDateTime' => $startTime,
            'endDateTime' => $endTime
        ]);
        dd($event);
        return $event;
    }
    public function destroy($eventId)
    {
       return Event::find($eventId)->delete();
    }
}