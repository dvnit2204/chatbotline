<?php
namespace App\Services;

use Illuminate\Support\Facades\Storage;
use LINE\LINEBot\RichMenuBuilder\RichMenuSizeBuilder;
use LINE\LINEBot\RichMenuBuilder\RichMenuAreaBuilder;
use LINE\LINEBot\RichMenuBuilder\RichMenuAreaBoundsBuilder;
use LINE\LINEBot\TemplateActionBuilder\MessageTemplateActionBuilder;
use Intervention\Image\ImageManagerStatic as Image;

use LINE\LINEBot\Constant\HTTPHeader;
use LINE\LINEBot\Event\MessageEvent;
use LINE\LINEBot\Event\MessageEvent\TextMessage;
use LINE\LINEBot\Exception\InvalidEventRequestException;
use LINE\LINEBot\Exception\InvalidSignatureException;

class LineService
{
    public function createRichMenu($params)
    {
        $accessToken = config('services.line.token');
        $channelSecret = config('services.line.secret');
        $httpClient = new \LINE\LINEBot\HTTPClient\CurlHTTPClient($accessToken);
        $bot = new \LINE\LINEBot($httpClient, ['channelSecret' => $channelSecret]);
        $richMenuBuilder = new \LINE\LINEBot\RichMenuBuilder(RichMenuSizeBuilder::getFull(), $params['selected'], $params['name'], $params['chatBarText'], [
            new RichMenuAreaBuilder(
                new RichMenuAreaBoundsBuilder(0, 10, 1250, 1676),
                new MessageTemplateActionBuilder('message label', 'test message')
            ),
            new RichMenuAreaBuilder(
                new RichMenuAreaBoundsBuilder(1250, 0, 1240, 1686),
                new MessageTemplateActionBuilder('message label 2', 'test message 2')
            )
        ]);
        $response = $bot->createRichMenu($richMenuBuilder);
        return json_decode($response->getRawBody());
    }
    public function uploadFile($file , $path = null)
    {
        $publicPath = public_path($path);
        if (!\File::exists($publicPath)) {
            \File::makeDirectory($publicPath, 0775, true, true);
        }
        $name = time().$file->getClientOriginalName();
        $name = preg_replace('/\s+/', '', $name);
        $image_resize = Image::make($file->getRealPath());     
        $image_resize->resize(800, 250);
        $image_resize->save(public_path('images/' .$name));
        return $path.'/'.$image_resize->basename;
    }

    public function uploadImageRichMenu($filePathImage, $menuId)
    {
        $accessToken = config('services.line.token');
        $channelSecret = config('services.line.secret');
        $httpClient = new \LINE\LINEBot\HTTPClient\CurlHTTPClient($accessToken);
        $bot = new \LINE\LINEBot($httpClient, ['channelSecret' => $channelSecret]);
        $pathImage = $filePathImage;
        $contentType = 'image/jpeg';
        $response = $bot->uploadRichMenuImage($menuId, $pathImage, $contentType);
        return $response;
    }

    public function listMenu()
    {
        $accessToken = '9TaKU4rPQjfhWNqw2O+ltYgSvg5N0p8OxCAzfn+hbw1gO0m5rXKENS/TsZDqqHfol3L924A8j193n2hz8qggDLFB9wuIstJvX+0jirERXC6YGgj4mKniCNgqVOpeov95q+9z+xSwXJVpmKtX2MHrZAdB04t89/1O/w1cDnyilFU=';
        $channelSecret = 'e45cd3dc7cac342027208c504cb5ae5b';
        $httpClient = new \LINE\LINEBot\HTTPClient\CurlHTTPClient($accessToken);
        $bot = new \LINE\LINEBot($httpClient, ['channelSecret' => $channelSecret]);
        $response = $bot->getRichMenuList();
        return json_decode($response->getRawBody());
    }
    public function detailMenu($menuId)
    {
        $accessToken = config('services.line.token');
        $channelSecret = config('services.line.secret');
        $httpClient = new \LINE\LINEBot\HTTPClient\CurlHTTPClient($accessToken);
        $bot = new \LINE\LINEBot($httpClient, ['channelSecret' => $channelSecret]);
        $response = $bot->setDefaultRichMenuId($menuId);
        dd($response);
        return $response;
    }
    public function deleteMenu($menuId)
    {
        $accessToken = config('services.line.token');
        $channelSecret = config('services.line.secret');
        $httpClient = new \LINE\LINEBot\HTTPClient\CurlHTTPClient($accessToken);
        $bot = new \LINE\LINEBot($httpClient, ['channelSecret' => $channelSecret]);
        $response = $bot->deleteRichMenu($menuId);
        dd($response);
        return $response;
    }

    public function repMessage()
    {
        $accessToken = '9TaKU4rPQjfhWNqw2O+ltYgSvg5N0p8OxCAzfn+hbw1gO0m5rXKENS/TsZDqqHfol3L924A8j193n2hz8qggDLFB9wuIstJvX+0jirERXC6YGgj4mKniCNgqVOpeov95q+9z+xSwXJVpmKtX2MHrZAdB04t89/1O/w1cDnyilFU=';
        $channelSecret = 'e45cd3dc7cac342027208c504cb5ae5b';
        $data = file_get_contents('https://eofkmifggb5wxux.m.pipedream.net/webhook');
        // dd($data);
        $httpClient = new \LINE\LINEBot\HTTPClient\CurlHTTPClient($accessToken);
        $bot = new \LINE\LINEBot($httpClient, ['channelSecret' => $channelSecret]);
        $textMessageBuilder = new \LINE\LINEBot\MessageBuilder\TextMessageBuilder('Toi day');
        // dd($textMessageBuilder);
        $response = $bot->replyMessage('8c9cf5cc11424539881d2bb2ae701f27', $textMessageBuilder);
        echo $response->getHTTPStatus() . ' ' . $response->getRawBody();
    }
}

