<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use \LINE\LINEBot\SignatureValidator as SignatureValidator;

class ChatBotCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'chatbot:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $accessToken = '9TaKU4rPQjfhWNqw2O+ltYgSvg5N0p8OxCAzfn+hbw1gO0m5rXKENS/TsZDqqHfol3L924A8j193n2hz8qggDLFB9wuIstJvX+0jirERXC6YGgj4mKniCNgqVOpeov95q+9z+xSwXJVpmKtX2MHrZAdB04t89/1O/w1cDnyilFU=';
        $channelSecret = 'e45cd3dc7cac342027208c504cb5ae5b';
        // get request body and line signature header
        $body = file_get_contents('php://input');
        // $signature = \Request::header('HTTP_X_LINE_SIGNATURE');
        $hash = hash_hmac('sha256', $body, $channelSecret, true);
        $signature = base64_encode($hash);
        \Log::debug($signature);
        // log body and signature
        file_put_contents('php://stderr', 'Body: '.$body);

        // is LINE_SIGNATURE exists in request header?
        if (empty($signature)){
            // return $response->withStatus(400, 'Signature not set');
        }

        // is this request comes from LINE?
        if(env('PASS_SIGNATURE') == false && ! SignatureValidator::validateSignature($body, $channelSecret, $signature)){
            // return $response->withStatus(400, 'Invalid signature');
        }
        // init bot
        $httpClient = new \LINE\LINEBot\HTTPClient\CurlHTTPClient($accessToken);
        $bot = new \LINE\LINEBot($httpClient, ['channelSecret' => $channelSecret]);

        $data = json_decode($body, true);
        \Log::debug($data . "data");
        if(!empty($data)){
            foreach ($data['events'] as $event)
            {
                if ($event['type'] == 'message')
                {
                    if($event['message']['type'] == 'text')
                    {
                        // send same message as reply to user
                        $result = $bot->replyText($event['replyToken'], $event['message']['text']);
                        // or we can use pushMessage() instead to send reply message
                        return $result->getHTTPStatus() . ' ' . $result->getRawBody();
                    }
                }
            }
        }
       

    }
}
