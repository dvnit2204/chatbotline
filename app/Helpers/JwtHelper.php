<?php

namespace App\Helpers;

use Ahc\Jwt\JWT;
use Carbon\Carbon;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Facades\JWTFactory;

class JwtHelper
{
    static private $key = 'example_key';

    static private $payload = [
        'iss' => 'http://example.org',
        'aud' => 'http://example.com',
        'iat' => 1356999524,
        'nbf' => 1357000000,
    ];

    public static function encode()
    {
        return JWT::encode(self::$payload, self::$key, 'HS256');
    }

    public static function testkid()
    {
        $user = [
            'iss' => 1234,
            'iat' => 'acb',
            'exp' => Carbon::now()->timestamp,
            'nbf' => 'acb',
            'sub' => 'abc',
            'jti' => 'abc'
        ];
        $payload = JWTFactory::make($user);
        $token = JWTAuth::encode($payload);
        return $token;
    }
}
