<?php

namespace App\Http\Controllers;

use App\Services\LineService;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use LINE\LINEBot\EchoBot\Setting;

class RichMenuLineController extends Controller
{
    protected $lineService;
    public function __construct(LineService $lineService)
    {
        $this->lineService = $lineService;
    }
    public function createMenu(Request $request)
    {
        $result = $this->lineService->createRichMenu($request->all());
        dd($result);
    }
    public function getListMenu()
    {
         dd($this->lineService->listMenu());
    }
    public function detailMenu()
    {
        $menuId = 'richmenu-05ff30949e08de91ec8dfaf7cb05f35c';
        return $this->lineService->detailMenu($menuId);
    }
    public function uploadImageRichMenu(Request $request)
    {
        $menuId = 'richmenu-6ed6a779bef41663aa57bf2f638c91ed';
        $data = '';
        if($file = $request->file('image')) {
            $filePathImage = $this->lineService->uploadFile($file,'images');
            $data = $this->lineService->uploadImageRichMenu($filePathImage, $menuId);
        }
        dd($data);
        return $data;
    }
    public function deleteMenu()
    {
        $menuId = 'richmenu-5e256d9092147248b1ca19d4b8f4e6d8';
        return $this->lineService->deleteMenu($menuId);
    }

    public function repMessage()
    {
        $httpClient = new \LINE\LINEBot\HTTPClient\CurlHTTPClient('<channel access token>');
        $bot = new \LINE\LINEBot($httpClient, ['channelSecret' => '<channel secret>']);
        $column1 = [];
        $textMessageBuilder = new \LINE\LINEBot\MessageBuilder\TemplateBuilder\CarouselTemplateBuilder($column1);
        $response = $bot->replyMessage('', $textMessageBuilder);
        echo $response->getHTTPStatus() . ' ' . $response->getRawBody();
    }
    public function testWebhook()
    {
        $accessToken = '9TaKU4rPQjfhWNqw2O+ltYgSvg5N0p8OxCAzfn+hbw1gO0m5rXKENS/TsZDqqHfol3L924A8j193n2hz8qggDLFB9wuIstJvX+0jirERXC6YGgj4mKniCNgqVOpeov95q+9z+xSwXJVpmKtX2MHrZAdB04t89/1O/w1cDnyilFU=';
        $channelSecret = 'e45cd3dc7cac342027208c504cb5ae5b';
     
        $httpClient = new \LINE\LINEBot\HTTPClient\CurlHTTPClient($accessToken);
        $bot = new \LINE\LINEBot($httpClient, ['channelSecret' => $channelSecret]);
        $data = $bot->getBotInfo();
        return $data->getHTTPStatus() . ' ' . $data->getRawBody();
    }

    public function webhook(Request $request)
    {
        Log::info('trigger event line');
        return response()->json([
            'code' => 200,
            'msg'  => 'success'
        ], 200);
    }
}
