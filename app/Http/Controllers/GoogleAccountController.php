<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GoogleAccountController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('accounts', [
            'accounts' => auth()->user()->googleAccounts,
        ]);
    }

    public function store()
    {
        // TODO:
        // - Handle the OAuth process.
        // - Create a new Google Account.
    }

    public function destroy()
    {
        // TODO:
        // - Revoke the authentication token.
        // - Delete the Google Account.
    }
}
