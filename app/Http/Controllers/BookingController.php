<?php

namespace App\Http\Controllers;

use App\Http\Requests\CalendarRequest;
use App\Services\BookingService;
use Illuminate\Http\Request;

class BookingController extends Controller
{
    protected $bookingService;
    public function __construct(BookingService $bookingService)
    {
        $this->bookingService = $bookingService;
    }

    public function listCalendar()
    {
        return $this->bookingService->list();
    }
    public function createCalendar(CalendarRequest $request)
    {
        return $this->bookingService->create($request->all());
    }
    public function updateCalendar(CalendarRequest $request)
    {
        return $this->bookingService->update($request->id, $request->all());
    }

    public function deleteCalendar(Request $request)
    {
        return $this->bookingService->destroy($request->id);
    }
}
