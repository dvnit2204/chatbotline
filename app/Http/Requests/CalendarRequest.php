<?php

namespace App\Http\Requests;

use App\Rules\checkDateRule;
use App\Rules\checkTimeRule;
use Illuminate\Foundation\Http\FormRequest;

class CalendarRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name" => ['required', 'string', 'max:255'],
            "meeting_date" => ['required', 'date', 'date_format:Y-m-d', new checkDateRule()],
            "meeting_time" => ['required', 'date_format:H:i']
        ];
    }
}
